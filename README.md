# Screenshots

## Customer form submission

### **Step 1**

![Step 1](images/form/1.introduction.png)

### **Step 2**

![Step 2](images/form/2.your-information.png)

### **Step 3**

![Step 3](images/form/3.beneficiary-information.png)

### **Step 4**

![Step 4](images/form/4.witness-information.png)

### **Step 5**

![Step 5](images/form/5.details-summary.png)

### **Step 6**

![Step 6](images/form/6.purchase-summary.png)

### **Step 7**

![Step 7](images/form/7.payment-status.png)

### **Step 8**

![Step 8](images/form/8.kyc-submission.png)

### **Step 9**

![Step 9](images/form/9.terms.png)

### **Step 10**

![Step 10](images/form/10.confirmation-of-will.png)

### **Step 11**

![Step 11](images/form/11.select-branch.png)

### **Step 12**

![Step 12](images/form/12.complete.png)

## Admin pages

### **Admin log-in**

![Log-in](images/admin/agent-login.png)

### **Agent info**

![Agent info](images/admin/agent-info.png)

### **Agent menu**

![Agent menu](images/admin/agent-menu.png)

### **Check status**

![Check status](images/admin/check-wasiat-status.png)

### **Reports**

![Reports](images/admin/admin-reports.png)

### **Manage user & role**

![Manage User & Role](images/admin/manage-users.png)
