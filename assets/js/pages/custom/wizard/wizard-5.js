/******/ (() => {
  // webpackBootstrap
  /******/ 'use strict';
  var __webpack_exports__ = {};
  /*!*******************************************************!*\
  !*** ../demo1/src/js/pages/custom/wizard/wizard-5.js ***!
  \*******************************************************/

  // Class definition
  var KTWizard5 = (function () {
    // Base elements
    var _wizardEl;
    var _formEl;
    var _wizardObj;
    var _validations = [];

    // Private functions
    var _initWizard = function () {
      // Initialize form wizard
      _wizardObj = new KTWizard(_wizardEl, {
        startStep: 1, // initial active step number
        clickableSteps: false, // allow step clicking
      });

      // Validation before going to next page
      _wizardObj.on('change', function (wizard) {
        if (wizard.getStep() > wizard.getNewStep()) {
          return; // Skip if stepped back
        }

        // Validate form before change wizard step
        var validator = _validations[wizard.getStep() - 1]; // get validator for currnt step

        if (validator) {
          validator.validate().then(function (status) {
            if (status == 'Valid') {
              wizard.goTo(wizard.getNewStep());

              KTUtil.scrollTop();
            } else {
              Swal.fire({
                text:
                  'Sorry, looks like there are some errors detected, please try again.',
                icon: 'error',
                buttonsStyling: false,
                confirmButtonText: 'Ok, got it!',
                customClass: {
                  confirmButton: 'btn font-weight-bold btn-light',
                },
              }).then(function () {
                KTUtil.scrollTop();
              });
            }
          });
        }

        return false; // Do not change wizard step, further action will be handled by he validator
      });

      // Change event
      _wizardObj.on('changed', function (wizard) {
        KTUtil.scrollTop();
      });

      // Submit event
      _wizardObj.on('submit', function (wizard) {
        Swal.fire({
          text: 'All is good! Please confirm the form submission.',
          icon: 'success',
          showCancelButton: true,
          buttonsStyling: false,
          confirmButtonText: 'Yes, submit!',
          cancelButtonText: 'No, cancel',
          customClass: {
            confirmButton: 'btn font-weight-bold btn-primary',
            cancelButton: 'btn font-weight-bold btn-default',
          },
        }).then(function (result) {
          if (result.value) {
            _formEl.submit(); // Submit form
          } else if (result.dismiss === 'cancel') {
            Swal.fire({
              text: 'Your form has not been submitted!.',
              icon: 'error',
              buttonsStyling: false,
              confirmButtonText: 'Ok, got it!',
              customClass: {
                confirmButton: 'btn font-weight-bold btn-primary',
              },
            });
          }
        });
      });
    };

    var _initValidation = function () {
      // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/

      // Step 1
      _validations.push(
        FormValidation.formValidation(_formEl, {
          fields: {},
          plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            // Bootstrap Framework Integration
            bootstrap: new FormValidation.plugins.Bootstrap({
              //eleInvalidClass: '',
              eleValidClass: '',
            }),
          },
        })
      );

      // Step 2
      _validations.push(
        FormValidation.formValidation(_formEl, {
          fields: {
            testatorIcNumber: {
              validators: {
                notEmpty: {
                  message: 'IC number field is required',
                },
              },
            },
            testatorFullName: {
              validators: {
                notEmpty: {
                  message: 'Full name field is required',
                },
              },
            },
            testatorGender: {
              validators: {
                notEmpty: {
                  message: 'Gender field is required',
                },
              },
            },
            testatorFullName: {
              validators: {
                notEmpty: {
                  message: 'Full name field is required',
                },
              },
            },
            testatorAddressLine1: {
              validators: {
                notEmpty: {
                  message: 'Address 1 field is required',
                },
              },
            },
            testatorAddressLine2: {
              validators: {
                notEmpty: {
                  message: 'Address 2 field is required',
                },
              },
            },
            testatorPostcode: {
              validators: {
                notEmpty: {
                  message: 'Postcode field is required',
                },
              },
            },
            testatorState: {
              validators: {
                notEmpty: {
                  message: 'State field is required',
                },
              },
            },
            testatorTelephoneNumber: {
              validators: {
                notEmpty: {
                  message: 'Telephone number field is required',
                },
              },
            },
            testatorMobileNumber: {
              validators: {
                notEmpty: {
                  message: 'Mobile number field is required',
                },
              },
            },
            testatorEmail: {
              validators: {
                notEmpty: {
                  message: 'Email address field is required',
                },
              },
            },
            testatorCitizenship: {
              validators: {
                notEmpty: {
                  message: 'Citizenship field is required',
                },
              },
            },
            testatorReligion: {
              validators: {
                notEmpty: {
                  message: 'Religion field is required',
                },
              },
            },
            testatorOccupation: {
              validators: {
                notEmpty: {
                  message: 'Occupation field is required',
                },
              },
            },
          },
          plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            // Bootstrap Framework Integration
            bootstrap: new FormValidation.plugins.Bootstrap({
              //eleInvalidClass: '',
              eleValidClass: '',
            }),
          },
        })
      );

      // Step 3
      _validations.push(
        FormValidation.formValidation(_formEl, {
          fields: {
            beneficiaryFullName: {
              validators: {
                notEmpty: {
                  message: 'Full name field is required',
                },
              },
            },
            beneficiaryIcNumber: {
              validators: {
                notEmpty: {
                  message: 'IC number field is required',
                },
              },
            },
            beneficiaryAddressLine1: {
              validators: {
                notEmpty: {
                  message: 'Address 1 field is required',
                },
              },
            },
            beneficiaryAddressLine2: {
              validators: {
                notEmpty: {
                  message: 'Address 2 field is required',
                },
              },
            },
            beneficiaryPostcode: {
              validators: {
                notEmpty: {
                  message: 'Postcode field is required',
                },
              },
            },
            beneficiaryState: {
              validators: {
                notEmpty: {
                  message: 'State field is required',
                },
              },
            },
            beneficiaryTelephoneNumber: {
              validators: {
                notEmpty: {
                  message: 'Telephone number field is required',
                },
              },
            },
            beneficiaryMobileNumber: {
              validators: {
                notEmpty: {
                  message: 'Mobile number field is required',
                },
              },
            },
            beneficiaryEmail: {
              validators: {
                notEmpty: {
                  message: 'Email address field is required',
                },
              },
            },
            beneficiaryRelationship: {
              validators: {
                notEmpty: {
                  message: 'Relationship field is required',
                },
              },
            },
          },
          plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            // Bootstrap Framework Integration
            bootstrap: new FormValidation.plugins.Bootstrap({
              //eleInvalidClass: '',
              eleValidClass: '',
            }),
          },
        })
      );

      // Step 4
      _validations.push(
        FormValidation.formValidation(_formEl, {
          fields: {
            witness1FullName: {
              validators: {
                notEmpty: {
                  message: 'Full name field is required',
                },
              },
            },
            witness1IcNumber: {
              validators: {
                notEmpty: {
                  message: 'IC number field is required',
                },
              },
            },
            witness1Email: {
              validators: {
                notEmpty: {
                  message: 'Email address field is required',
                },
              },
            },
            witness1TelephoneNumber: {
              validators: {
                notEmpty: {
                  message: 'Telephone number field is required',
                },
              },
            },
            witness1MobileNumber: {
              validators: {
                notEmpty: {
                  message: 'Mobile number field is required',
                },
              },
            },
            witness1AddressLine1: {
              validators: {
                notEmpty: {
                  message: 'Address 1 field is required',
                },
              },
            },
            witness1AddressLine2: {
              validators: {
                notEmpty: {
                  message: 'Address 2 field is required',
                },
              },
            },
            witness1Postcode: {
              validators: {
                notEmpty: {
                  message: 'Postcode field is required',
                },
              },
            },
            witness1State: {
              validators: {
                notEmpty: {
                  message: 'State field is required',
                },
              },
            },
            witness2FullName: {
              validators: {
                notEmpty: {
                  message: 'Full name field is required',
                },
              },
            },
            witness2IcNumber: {
              validators: {
                notEmpty: {
                  message: 'IC number field is required',
                },
              },
            },
            witness2Email: {
              validators: {
                notEmpty: {
                  message: 'Email address field is required',
                },
              },
            },
            witness2TelephoneNumber: {
              validators: {
                notEmpty: {
                  message: 'Telephone number field is required',
                },
              },
            },
            witness2MobileNumber: {
              validators: {
                notEmpty: {
                  message: 'Mobile number field is required',
                },
              },
            },
            witness2AddressLine1: {
              validators: {
                notEmpty: {
                  message: 'Address 1 field is required',
                },
              },
            },
            witness2AddressLine2: {
              validators: {
                notEmpty: {
                  message: 'Address 2 field is required',
                },
              },
            },
            witness2Postcode: {
              validators: {
                notEmpty: {
                  message: 'Postcode field is required',
                },
              },
            },
            witness2State: {
              validators: {
                notEmpty: {
                  message: 'State field is required',
                },
              },
            },
          },
          plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            // Bootstrap Framework Integration
            bootstrap: new FormValidation.plugins.Bootstrap({
              //eleInvalidClass: '',
              eleValidClass: '',
            }),
          },
        })
      );
      // Step 5
      _validations.push(
        FormValidation._formValidation(_formEl, {
          fields: {},
          plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            // Bootstrap Framework Integration
            bootstrap: new FormValidation.plugins.Bootstrap({
              //eleInvalidClass: '',
              eleValidClass: '',
            }),
          },
        })
      );
    };

    return {
      // public functions
      init: function () {
        _wizardEl = KTUtil.getById('kt_wizard');
        _formEl = KTUtil.getById('kt_form');

        _initWizard();
        _initValidation();
      },
    };
  })();

  jQuery(document).ready(function () {
    KTWizard5.init();
  });

  /******/
})();
//# sourceMappingURL=wizard-5.js.map
